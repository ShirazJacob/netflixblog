﻿function updateSeriesRate() {
    $(function () {
        var seriesTitles = $(".card-body-heading").children();
        var imdbVotes = $(".imdbVoters");
        var imdbRate = $(".imdbRating");

        for (var i = 0; i < seriesTitles.length; i++) {
            getRatingFromIMDB(i, seriesTitles, imdbRate,  imdbVotes);
        }
    });

}

function getRatingFromIMDB(i, seriesTitles, imdbRate, imdbVotes) {
    $.ajax({
        url: "https://www.omdbapi.com/",
        headers: {
            "Accept": "application/json"
        },
        async: false,
        type: "GET",
        data: { "t": seriesTitles[i].text, "apikey": "b0b898b4" },
        success: function (response) {
            if (response.error) {
                return;
            }

            if (isNaN(response.imdbRating)) {
                imdbRate[i].outerText = "rating is unavaliable";
            } else {
                imdbRate[i].outerText = response.imdbRating;
                imdbVotes[i].outerText = "out of " + response.imdbVotes + " voters";
            }
        },
        error: function (response) {
            alert('Error was occured, please try again later', response)
        }
    });
}