﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using NetflixBlog.Data;
using NetflixBlog.Models;
using ActionResult = Microsoft.AspNetCore.Mvc.ActionResult;
using Controller = Microsoft.AspNetCore.Mvc.Controller;
using SelectListItem = Microsoft.AspNetCore.Mvc.Rendering.SelectListItem;

namespace NetflixBlog.Controllers
{
    [Microsoft.AspNetCore.Authorization.Authorize(Roles = "Client, Admin")]
    public class PostsController : Controller
    {
        private readonly NetflixBlogContext _context;

        public PostsController(NetflixBlogContext context)
        {
            _context = context;
        }


        // GET: Posts
        public async Task<ActionResult> Index()
        {
            return View(await _context.Post.ToListAsync());
        }

        // GET: Posts/Details/5
        public ActionResult Details(int? id, int? series)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Post post = _context.Post.Find(id);
            if (post == null)
            {
                return NotFound();
            }
            return View(post);
        }

        // GET: Posts/Create
        public ActionResult Create()
        {
            //var allSeries = _context.Series;
            ////this.ViewData["seriesSelectable"] = (IEnumerable<Models.Series>)allSeries;

            //var allPosts = _context.Post;
            //PostCreation pc = new PostCreation();
            ////pc.SeriesNamesList = allSeries;

            //foreach (var series in allSeries)
            //{
            //    //pc.SeriesNamesList.Add(series.Name); 
            //    pc.Series.Add(series);
            //}

            //this.ViewData["seriesSelectable"] = (IEnumerable<PostCreation>) pc;
            // this.ViewData["Create"] = pc;


            List<SelectListItem> series = new List<SelectListItem>();

            // Build the select list of all the possible Series
            foreach (Series series1 in _context.Series.ToList())
            {
                series.Add(new SelectListItem { Text = series1.Name, Value = series1.SeriesID.ToString() });
            }

            ViewBag.Series = series;

            return View();
        }

        // POST: Posts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Microsoft.AspNetCore.Mvc.HttpPost]
        [Microsoft.AspNetCore.Mvc.ValidateAntiForgeryToken]
        public ActionResult Create([Microsoft.AspNetCore.Mvc.Bind("PostID,Title,AuthorName,SeriesID,Content")] Post post, string category, string Series)
        {
            post.Date = DateTime.Now;
            if (ModelState.IsValid)
            {
                Category newCategory;
                //if (string.IsNullOrEmpty(category) && string.IsNullOrEmpty(Series))
                //{
                //    _context.Post.Add(post);
                //    _context.SaveChanges();

                //    return View(post);

                //}
                //else
                //  {
                if (!string.IsNullOrEmpty(category))
                {
                    Enum.TryParse(category, out newCategory);
                    post.Category = newCategory;
                }

                if (!string.IsNullOrEmpty(Series))
                {
                    post.SeriesId = Int32.Parse(Series);
                    //var series = _context.Series.Where(s => s.SeriesID == seriesID);
                }

                _context.Post.Add(post);
                _context.SaveChanges();

                int prediction;
                int.TryParse(category, out prediction);

                return RedirectToAction("Index");
                // }
            }

            var allSeries = _context.Series;
            this.ViewData["seriesSelectable"] = (IEnumerable<Models.Series>)allSeries;
            return View(post);
        }

        public ActionResult Approve(int? id, Category category)
        {
            Post post = _context.Post.Find(id);
            _context.Entry(post).State = EntityState.Modified;
            post.Category = category;
            _context.SaveChanges();

            int prediction;
            int.TryParse(category.GetHashCode().ToString(), out prediction);

            return RedirectToAction("Index");
        }

        [Microsoft.AspNetCore.Mvc.HttpPost]
        [Microsoft.AspNetCore.Mvc.ValidateAntiForgeryToken]
        public ActionResult Suggestion([Microsoft.AspNetCore.Mvc.Bind("PostID")] int? PostID, string category)
        {
            Category newCategory;

            if (!string.IsNullOrEmpty(category) && Enum.TryParse(category, out newCategory))
            {
                Approve(PostID, newCategory);
            }

            return RedirectToAction("Index");
        }

        // GET: Posts/Edit/5
        [Microsoft.AspNetCore.Authorization.Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Post post = _context.Post.Find(id);
            if (post == null)
            {
                return NotFound();
            }

            var allSeries = _context.Series;
            this.ViewData["seriesSelectable"] = (IEnumerable<Models.Series>)allSeries;

            List<SelectListItem> series = new List<SelectListItem>();

            // Build the select list of all the possible Series
            foreach (Series series1 in allSeries.ToList())
            {
                series.Add(new SelectListItem { Text = series1.Name, Value = series1.SeriesID.ToString() });
            }

            ViewBag.Series = series;


            return View(post);
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Microsoft.AspNetCore.Mvc.HttpPost]
        [Microsoft.AspNetCore.Mvc.ValidateAntiForgeryToken]
        public ActionResult Edit([Microsoft.AspNetCore.Mvc.Bind("PostID,Title,AuthorName,SeriesID,Series,Date,Content")] Post post, string category, string Series)
        {
            if (ModelState.IsValid)
            {
                _context.Entry(post).State = EntityState.Modified;
                Category newCategory;
                Enum.TryParse(category, out newCategory);
                post.Category = newCategory;
                post.SeriesId = Int32.Parse(Series);
                _context.Entry(post).State = EntityState.Modified;
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            var allSeries = _context.Series;
            this.ViewData["seriesSelectable"] = (IEnumerable<Models.Series>)allSeries;
            return View(post);
        }

        // GET: Posts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Post post = _context.Post.Find(id);
            if (post == null)
            {
                return NotFound();
            }
            return View(post);
        }

        // POST: Posts/Delete/5
        [Microsoft.AspNetCore.Mvc.HttpPost, Microsoft.AspNetCore.Mvc.ActionName("Delete")]
        [Microsoft.AspNetCore.Mvc.ValidateAntiForgeryToken]
        [Microsoft.AspNetCore.Authorization.Authorize(Roles = "Admin")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Post post = await _context.Post.FindAsync(id);
            _context.Post.Remove(post);
            IQueryable<Comment> comments =  _context.Comment.Where(c => c.PostID == id);
            foreach (var c in comments)
            {
                _context.Comment.Remove(c);
            }
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Posts
        public ActionResult Filter(string seriesGenre, string authorName, string seriesName, string postCategory)
        {
            // var results = _context.Post.AsQueryable();
            var results = _context.Post.Include(p => p.Series).AsQueryable();
            Genre genre;
            Category category;
            if (!string.IsNullOrEmpty(seriesName))
            {
                results = _context.Post
                    .Join(_context.Series, post => post.SeriesId, series => series.SeriesID,
                        (post, series) => new { post, series })
                    .Where(@p => @p.series.Name == seriesName)
                    .Select(@t => @t.post);
            }
            if (!string.IsNullOrEmpty(authorName))
            {
                results = results.Where(p => p.AuthorName.Contains(authorName));
            }
            if (!string.IsNullOrEmpty(postCategory) && Enum.TryParse(postCategory, out category))
            {
                results = results.Where(m => m.Category == category);
            }
            return View("Filter", results.ToList());
        }

        public async Task<ActionResult> GroupBySeries()
        {
            // Group by and join
            //IQueryable<SelectBySeries> postsAmount = _context.Post.GroupBy(post => post.SeriesId)
            //    .Join(_context.Series, g => g.Key, series => series.SeriesID,
            //        (g, series) => new SelectBySeries() { SeriesName = series.Name, PostsAmount = g.Sum(m => 1) });

           

            //var source = _context.Post
            //    .Select(m => new
            //    {
            //        Key = new
            //        {
            //            SeriesID = m.SeriesId
            //        },
            //        Post = m
            //    });


            var posts = await _context.Post.ToListAsync();
            var series = await _context.Series.ToListAsync();
            List<SelectBySeries> postsAmount = (from s in series let postCount = posts.Count(p => s.SeriesID == p.SeriesId) select new SelectBySeries {SeriesName = s.Name, SeriesId = s.SeriesID, PostsAmount = postCount}).ToList();
            return View(postsAmount);


            //return View(postsAmount);
        }

        [Microsoft.AspNetCore.Mvc.HttpGet]
        public ActionResult GroupBySeriesData()
        {
            // Group by and join
            var postsAmount = _context.Post.GroupBy(post => post.SeriesId)
                .Join(_context.Series, g => g.Key, series => series.SeriesID,
                    (g, series) => new SelectBySeries() { SeriesName = series.Name, PostsAmount = g.Sum(m => 1) });

            return Json(postsAmount.ToList(), JsonRequestBehavior.AllowGet);
        }

        // Get Posts/WantMore
        [Microsoft.AspNetCore.Mvc.HttpPost]
        public ActionResult WantMore(string seriesId, string currentPostId)
        {
            try
            {
                int postId = Int32.Parse(currentPostId);
                int serId = Int32.Parse(seriesId);
                var alikePost = (_context.Post.Where(post => post.PostID != postId && post.SeriesId == serId)
                    .Select(post => post.PostID)).SingleOrDefault();
                if (alikePost != 0)
                {
                    return Json(new Dictionary<string, object> { { "url", "/Posts/Details/" + alikePost + "?series=" + seriesId } });
                }
                else
                {
                    return Json(new Dictionary<string, object> { { "error", "didnt found any" } });
                }
            }
            catch (Exception ex)
            {
                return Json(new Dictionary<string, object> { { "error", ex.Message } });
            }


        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}