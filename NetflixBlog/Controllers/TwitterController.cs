﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Tweetinvi.Models;
using System;
using Tweetinvi;
using NetflixBlog.Models;

namespace NetflixBlog.Controllers
{
    [Authorize(Roles = "Client, Admin")]
    public class TwitterController : Controller
    {
        private const string TWITTER_CONSUMER_ID = "AVd5RrV0QpDClKkGdz9J9F6Nm";
        private const string TWITTER_CONSUMER_SECRET = "0CoQOMKgs8fb4jhFPVlMGELE07vpiiD80kGYKTZM1OKYkReRgO";
        private const string TWITTER_ACCESS_TOKEN = "1429534747890536448-cHJePwbFDErIUy9NMG1DFkVn9HEiGf";
        private const string TWITTER_ACCESS_TOKEN_SECRET = "oTAvcC6OmQ31Cr9LoNoDWR3FlcNMT0JOjB8rKqVARlGnU";

        // GET: Twitter/Tweet
        public ActionResult Create()
        {
            return View();
        }

        // POST Twitter/Tweet
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create(Tweet tweet)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userClient = new TwitterClient(TWITTER_CONSUMER_ID, TWITTER_CONSUMER_SECRET, TWITTER_ACCESS_TOKEN, TWITTER_ACCESS_TOKEN_SECRET);

                    // Publish a tweet
                    var publishedTweet = await userClient.Tweets.PublishTweetAsync(tweet.TweetText);
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Something went wrong, please try again";
                }
            }

            return View();
        }
    }
}
