﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NetflixBlog.Models;
using System.Diagnostics;

namespace NetflixBlog.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View("About");
        }

        [Authorize(Roles = "Client, Admin")]
        public IActionResult Privacy()
        {
            return View();
        }

        [Authorize(Roles = "Client, Admin")]
        public IActionResult About()
        {
            ViewBag.Message = "Netflix series Blog";

            return View();
        }

        [Authorize(Roles = "Client, Admin")]
        public IActionResult Contact()
        {
            ViewBag.Message = "Your contact page";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
