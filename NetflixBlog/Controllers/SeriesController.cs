﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NetflixBlog.Data;
using NetflixBlog.Models;
using System.IO;
using Microsoft.AspNetCore.Mvc.Rendering;
using Tweetinvi.Core.Extensions;
using Microsoft.AspNetCore.Authorization;

namespace NetflixBlog.Controllers
{
    [Authorize(Roles = "Client, Admin")]
    public class SeriesController : Controller
    {
        private readonly NetflixBlogContext _context;

        public SeriesController(NetflixBlogContext context)
        {
            _context = context;
        }

        // GET: seriess
        public async Task<IActionResult> Index(string searchName, string searchGenre, string searchReleaseDate, string searchLanguage)
        {
            var results = _context.Series.AsQueryable();
            int releaseDate;
            Genre genre;
            Language language;

            if (!string.IsNullOrEmpty(searchName))
            {
                results = results.Where(s => s.Name.Contains(searchName));
            }
            if (!string.IsNullOrEmpty(searchGenre) && Enum.TryParse(searchGenre, out genre))
            {
                results = results.Where(s => s.Genre == genre);
            }
            if (!string.IsNullOrEmpty(searchLanguage) && Enum.TryParse(searchLanguage, out language))
            {
                results = results.Where(s => s.Language == language);
            }
            if (!string.IsNullOrEmpty(searchReleaseDate) && int.TryParse(searchReleaseDate, out releaseDate))
            {
                results = results.Where(s => s.ReleaseDate.Year >= releaseDate);
            }
            
            return View(await results.ToListAsync());
        }

        [HttpGet]
        public IActionResult IndexData(string searchName, string searchGenre, string searchReleaseDate, string searchLanguage, double? searchRate)
        {
            var series = _context.Series.ToList();

            return Json(series);
        }

        public async Task<IActionResult> Trailer(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Series series = await _context.Series.FirstOrDefaultAsync(s => s.SeriesID == id);

            if (series == null)
            {
                return NotFound();
            }

            return View(series);
        }

        public IActionResult Pie()
        {
            return View();
        }

        // GET: seriessController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Series series = await _context.Series.Include(series => series.Actors).Include(series => series.Map).FirstOrDefaultAsync(s => s.SeriesID == id);
            
            if (series == null)
            {
                return NotFound();
            }


            List<SelectListItem> actors = new List<SelectListItem>();

            // Build the select list of all the possible actors
            foreach (Actor actor in _context.Actor.ToList())
            {
                if (series.Actors.Any(a => a.ActorId == actor.ActorId))
                {
                    actors.Add(new SelectListItem { Text = actor.FirstName + " " + actor.LastName, Value = actor.ActorId });
                }
            }

            ViewBag.Actors = actors;

            return View(series);

        }
        public IActionResult Create()
        {
            List<SelectListItem> actors = new List<SelectListItem>();
            
            // Build the select list of all the possible actors
            foreach (Actor actor in _context.Actor.ToList())
            {
                actors.Add(new SelectListItem { Text = actor.FirstName + " " + actor.LastName, Value = actor.ActorId });
            }

            ViewBag.Actors = actors;

            return View();
        }

        // POST: seriessController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Series series, string[] Actors)
        {
            if (ModelState.IsValid)
            {
                if (series.ImageFile != null)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        series.ImageFile.CopyTo(ms);
                        series.Image = ms.ToArray();
                    }
                }
         

                if(Actors.Length != 0)
                {
                    var selectActors = Actors.ToList();

                    series.Actors = _context.Actor.Where(actor => selectActors.Contains(actor.ActorId)).ToList();
                }

                _context.Series.Add(series);
                await _context.SaveChangesAsync();
                return View("Index", await _context.Series.ToListAsync());
            }

            return BadRequest();
        }
        public async Task<IActionResult> Edit(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var series = await _context.Series.Include(series => series.Actors).Include(series => series.Map).FirstOrDefaultAsync(s => s.SeriesID == id);

            List<SelectListItem> actors = new List<SelectListItem>();
            
            // Build the select list of all the possible actors
            foreach (Actor actor in _context.Actor.ToList())
            {
                actors.Add(new SelectListItem { Text = actor.FirstName + " " + actor.LastName, Value = actor.ActorId, Selected = series.Actors.Contains(actor) });
            }

            ViewBag.Actors = actors;

            if (series == null)
            {
                return NotFound();
            }
            return View(series);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        // GET: seriessController/Edit/series
        public async Task<IActionResult> Edit(int id, Series series, string[] Actors)
        {
            if (id != series.SeriesID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                series.Actors = new List<Actor>();

                try
                {
                    var old_series = await _context.Series.Include(series => series.Actors).Include(series => series.Map).FirstOrDefaultAsync(s => s.SeriesID == id);

                    // Delete the unrelevant actors before insert new 
                    if (old_series != null &&
                        !old_series.Actors.IsEmpty())
                    {
                        foreach (Actor actor in old_series.Actors)
                        {
                            old_series.Actors.Remove(actor);
                        }

                        await _context.SaveChangesAsync();
                    }

                    // After finish the delete detach the object
                    _context.Entry(old_series).State = EntityState.Detached;

                    // Check if actors were selected, get the relevant actors from the db
                    if (Actors.Length != 0)
                    {
                        var selectActors = Actors.ToList();

                        series.Actors = _context.Actor.Where(actor => selectActors.Contains(actor.ActorId)).ToList();
                    }

                    if (series.ImageFile != null)
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            series.ImageFile.CopyTo(ms);
                            series.Image = ms.ToArray();
                        }
                    }

                    if (old_series.Map != null)
                    {
                        series.Map.MapID = old_series.Map.MapID;
                        series.Map.SeriesId = old_series.Map.SeriesId;
                    }

                    _context.Series.Update(series);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SeriesExists(series.SeriesID))
                    {
                        return NotFound();
                    }

                    throw;
                }

                return View("Index", await _context.Series.ToListAsync());
            
            }

            return RedirectToAction(nameof(Edit));
        }

        public async Task<IActionResult> DeleteAsync(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var series = await _context.Series
                .FirstOrDefaultAsync(m => m.SeriesID == id);
            if (series == null)
            {
                return NotFound();
            }

            return View(series);
        }

        // GET: seriessController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            try
            {
                Series series = await _context.Series
                    .FirstOrDefaultAsync(s => s.SeriesID == id);

                if (series == null)
                {
                    return NotFound();
                }

               // _context.Series.Remove(series);


                if (series != null)
                {
                    IQueryable<Actor> actors = this._context.Actor.Include(a => a.Serieses)
                        .Where(a => a.Serieses.Contains(series));


                    foreach (Actor a in actors)
                    {
                        a.Serieses.Remove(series);
                    }

                    _context.Series.Remove(series);
                }


                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch(Exception ex)
            {
                throw;
            }

            return Ok();
        }

        // GET: Locations
        public async Task<IActionResult> Locations()
        {
            return View();
        }

        [HttpGet]
        public IActionResult LocationsData()
        {
            var series = _context.Series.Include(series => series.Map).Select(series => new { Location = series.Map.Location, SeriesName = series.Name }).AsNoTracking().ToList();
            return Json(series);
        }

        private bool SeriesExists(int id)
        {
            return _context.Series.Any(s => s.SeriesID == id);
        }
    }
}
