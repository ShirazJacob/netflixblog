﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NetflixBlog.Data;
using NetflixBlog.Models;
using System.Linq;
using System.Threading.Tasks;

namespace NetflixBlog.Controllers
{

    [Authorize(Roles = "Client, Admin")]
    public class CommentsController : Controller
    {
        private readonly NetflixBlogContext _context;

        public CommentsController(NetflixBlogContext context)
        {
            _context = context;
        }


        // GET: Comments
        public async Task<IActionResult> Index(string searchTitle, string searchAuthorName)
        {

            IQueryable<Comment> results = _context.Comment.AsQueryable().Include(p=>p.Post);
            if (!string.IsNullOrEmpty(searchTitle))
            {
                results = results.Where(s => s.Title.Contains(searchTitle));
            }
            if (!string.IsNullOrEmpty(searchAuthorName))
            {
                results = results.Where(s => s.AuthorName.Contains(searchAuthorName));
            }

            return View(await results.ToListAsync());

        }

        // GET: CommentsController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = await _context.Comment
                .Include(p => p.Post).FirstOrDefaultAsync(c => c.CommentID == id);
            if (comment == null)
            {
                return NotFound();
            }

            return View(comment);

        }



        // GET: Comments/Create
        public async Task<IActionResult> Add(int PostID)
        {
            var newComment = new Comment();
            newComment.PostID = PostID; // this will be sent from the ArticleDetails View, hold on :).

            return PartialView(newComment);
        }

        // POST: Comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add([Bind("CommentID,PostID,Title,AuthorName,Content")] Comment comment)
        {
            if (ModelState.IsValid)
            {
                await _context.Comment.AddAsync(comment);
                await _context.SaveChangesAsync();
                return Json(new { succeeded = true, commentData = comment });
            }

            ViewBag.PostID = new SelectList(_context.Post, "PostID", "Title", comment.PostID);
            return Json(new { succeeded = false });

        }

        // POST: Comments/Create
        public async Task<IActionResult> Create()
        {
            ViewBag.PostID = new SelectList(_context.Post, "PostID", "Title");
            return View();
        }

        // POST: Comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CommentID,PostID,Title,AuthorName,Content")] Comment comment)
        {
            if (ModelState.IsValid)
            {
                await _context.Comment.AddAsync(comment);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.PostID = new SelectList(_context.Post, "PostID", "Title", comment.PostID);
            return View(comment);
        }

/*        [HttpPost]
        [ValidateAntiForgeryToken]
        // GET: CommentsController/Edit/comment
        public async Task<IActionResult> Edit(int id, Comment comment)
        {
            if (id != comment.CommentID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Comment.Update(comment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CommentExists(comment.CommentID))
                    {
                        return NotFound();
                    }

                    throw;
                }
                return Ok();
            }

            return BadRequest();
        }
*/

        // GET: Comments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Comment comment = await _context.Comment.FindAsync(id);
            if (comment == null)
            {
                return NotFound();
            }
            ViewBag.PostID = new SelectList(_context.Post, "PostID", "Title", comment.PostID);
            return View(comment);
        }

        // POST: Comments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("CommentID,PostID,Title,AuthorName,Content")] Comment comment)
        {
            if (ModelState.IsValid)
            {
                _context.Entry(comment).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.PostID = new SelectList(_context.Post, "PostID", "Title", comment.PostID);
            return View(comment);
        }


        // GET: CommentsController/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }
        //    try
        //    {
        //        var comment = await _context.Comment
        //            .FirstOrDefaultAsync(c => c.CommentID == id);

        //        if (comment == null)
        //        {
        //            return NotFound();
        //        }

        //        _context.Comment.Remove(comment);
        //        await _context.SaveChangesAsync();
        //    }
        //    catch(Exception ex)
        //    {
        //        throw;
        //    }

        //    return Ok();
        //}


        // GET: Comments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Comment comment = _context.Comment.Find(id);
            if (comment == null)
            {
                return NotFound();
            }
            return View(comment);
        }

        // POST: Comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            Comment comment = await _context.Comment.FindAsync(id);
            _context.Comment.Remove(comment);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool CommentExists(int id)
        {
            return _context.Comment.Any(c => c.CommentID == id);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
