﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NetflixBlog.Migrations
{
    public partial class removeUnrelevant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Location_Latitude",
                table: "Series");

            migrationBuilder.DropColumn(
                name: "Location_Longtitude",
                table: "Series");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Map");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Location_Latitude",
                table: "Series",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Location_Longtitude",
                table: "Series",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Map",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
