﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NetflixBlog.Migrations
{
    public partial class addRelMap : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Post_Series_SeriesID",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "MovieId",
                table: "Post");

            migrationBuilder.RenameColumn(
                name: "SeriesID",
                table: "Post",
                newName: "SeriesId");

            migrationBuilder.RenameIndex(
                name: "IX_Post_SeriesID",
                table: "Post",
                newName: "IX_Post_SeriesId");

            migrationBuilder.AddColumn<int>(
                name: "SeriesId",
                table: "Map",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Map_SeriesId",
                table: "Map",
                column: "SeriesId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Map_Series_SeriesId",
                table: "Map",
                column: "SeriesId",
                principalTable: "Series",
                principalColumn: "SeriesID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Post_Series_SeriesId",
                table: "Post",
                column: "SeriesId",
                principalTable: "Series",
                principalColumn: "SeriesID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Map_Series_SeriesId",
                table: "Map");

            migrationBuilder.DropForeignKey(
                name: "FK_Post_Series_SeriesId",
                table: "Post");

            migrationBuilder.DropIndex(
                name: "IX_Map_SeriesId",
                table: "Map");

            migrationBuilder.DropColumn(
                name: "SeriesId",
                table: "Map");

            migrationBuilder.RenameColumn(
                name: "SeriesId",
                table: "Post",
                newName: "SeriesID");

            migrationBuilder.RenameIndex(
                name: "IX_Post_SeriesId",
                table: "Post",
                newName: "IX_Post_SeriesID");

            migrationBuilder.AddColumn<int>(
                name: "MovieId",
                table: "Post",
                type: "int",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Post_Series_SeriesID",
                table: "Post",
                column: "SeriesID",
                principalTable: "Series",
                principalColumn: "SeriesID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
