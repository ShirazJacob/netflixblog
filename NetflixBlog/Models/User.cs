﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Runtime.Versioning;
using System.Diagnostics.CodeAnalysis;

namespace NetflixBlog.Models
{
    public enum UserType
    {
        Client,
        Admin
    }

    public class User
    {

        [Key]
        [DisplayName("Username")]
        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Enter a password between 6 to 10 characters")]
        [MaxLength(10, ErrorMessage = "Enter a password between 6 to 10 characters")]
        public string Password { get; set; }

        [DisplayName("First name")]
        [MinLength(2, ErrorMessage =" First name must contain at least 2 characters")]
        public string FirstName { get; set; }

        [DisplayName("Last name")]
        [MinLength(2, ErrorMessage = " Last name must contain at least 2 characters")]
        public string LastName { get; set; }
         
        public UserType Type { get; set; } = UserType.Client;

    }
}
