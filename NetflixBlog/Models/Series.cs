﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;
using System.ComponentModel;
using System.Globalization;
using Microsoft.Extensions.Options;
using System.Web.Mvc;

namespace NetflixBlog.Models
{
    public enum Genre
    {
        Comedy, Action, Animated, Horror, ScienceFiction, Drama, Historical
    }

    public enum Language
    {
        English, France, Spanish, Hebrew, German
    }


    public class Series
    {
        [Key]
        public int SeriesID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public Genre Genre { get; set; }

        [Required]
        public string Producer { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = false)]
        [DisplayName("Release Date")]
        public DateTime ReleaseDate { get; set; }

        [DisplayName("Seasons Number")]
        [Range(1, Double.PositiveInfinity, ErrorMessage = "Season number must be bigger than 0")]
        public int SeasonsNums { get; set; }

        [DisplayName("Episode Number")]
        [Range(1, Double.PositiveInfinity, ErrorMessage = "Episode number must be bigger than 0")]
        public int EpisodesNums { get; set; }

        public Language Language { get; set; }

        public byte[] Image { get; set; }

        [NotMapped]
        [DisplayName("Image")]
        public IFormFile ImageFile { get; set; }

        [DisplayName("Trailer Path")]
        public string TrailerPath { get; set; }

        public virtual Map Map { get; set; }

        public virtual ICollection<Actor> Actors { get; set; }
    }
}
