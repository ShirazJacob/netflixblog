﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NetflixBlog.Models
{
    public class Tweet
    {
        [Required(ErrorMessage ="Please enter text to post tweet")]
        public string TweetText { get; set; }
    }
}
