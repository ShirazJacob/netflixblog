﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace NetflixBlog.Models
{
    public enum Category
    {
        NewSeries, Celebs, Reviews, BehindTheScene, NewsFlash
    }

    //public class PostCreation
    //{
    //    public IEnumerable<Post> Posts { get; set; }

    //    public IEnumerable<Series> Series { get; set; }



    //    public PostCreation()
    //    {
    //    }
    //}

    public class PostCreation
    {
        public string Title { get; set; }
        public string AuthorName { get; set; }
        public DateTime Date { get; set; }
        [BindProperty(SupportsGet = true)]
        public List<Series> Series { get; set; }
        public Category Category { get; set; }

        public string Content { get; set; }

        public string ImgPath { get; set; }


        public PostCreation()
        {
            Series = new List<Series>();
        }
    }



    public class Post
    {
        [Key]
        public int PostID { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string AuthorName { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime Date { get; set; }

        public int? SeriesId { get; set; }

        public Category Category { get; set; }

        public string Content { get; set; }

        public string ImgPath { get; set; }

        public virtual Series Series { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        
    }

    public class SelectBySeries
    {
        public int SeriesId { get; set; }
        public string SeriesName { get; set; }
        public int PostsAmount { get; set; }
    }

    public class SelectByCategory
    {
        public string CategoryName { get; set; }
        public int PostsAmount { get; set; }
    }
}
