﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace NetflixBlog.Models
{
    public class Map
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MapID { get; set; }

        public Coordinate Location { get; set; }

        public int SeriesId { get; set; }
        public virtual Series Series { get; set; }
    }

    [Owned]
    public class Coordinate
    {
        public double Longtitude { get; set; }
        public double Latitude { get; set; }
    }
}
