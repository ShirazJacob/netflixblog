﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NetflixBlog.Models;

namespace NetflixBlog.Data
{
    public class NetflixBlogContext : DbContext
    {
        public NetflixBlogContext (DbContextOptions<NetflixBlogContext> options)
            : base(options)
        {
        }

        public DbSet<NetflixBlog.Models.Map> Map { get; set; }
        public DbSet<NetflixBlog.Models.User> User { get; set; }
        public DbSet<NetflixBlog.Models.Comment> Comment { get; set; }
        public DbSet<NetflixBlog.Models.Series> Series { get; set; }
        public DbSet<NetflixBlog.Models.Post> Post { get; set; }
        public DbSet<NetflixBlog.Models.Actor> Actor { get; set; }


    }
}
